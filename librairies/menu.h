#ifndef MENU_H
#define MENU_H

int get_menu_choice(void);
int get_menu_salle(void);
int get_menu_reservation(void);
int get_menu_selectionresa(void);

#endif