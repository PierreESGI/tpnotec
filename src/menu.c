#include <stdio.h>
#include <stdlib.h>

int get_menu_choice(void)
{
    int selection = 0;
    do{
        system("clear");
        printf("\n1 - Gestion de la salle");
        printf("\n2 - Reservation des sieges");
        printf("\nChoissez votre action : ");
        scanf("%d", &selection );
    } while ((selection < 1) || (selection > 3));
        return selection;
}

int get_menu_salle(void)
{
    int selection = 0;
    do{
        system("clear");
        printf("\n1 - Modifier la salle");
        printf("\n2 - Importer une salle");
        printf("\n3 - Exporter une salle");
        printf("\nChoissez votre action : ");
        scanf("%d", &selection );
    } while ((selection < 1) || (selection > 3));
        return selection;
}

int get_menu_reservation(void)
{
    int selection = 0;
    do{
        system("clear");
        printf("\n1 - Effectuer une reservation");
        printf("\n2 - Voir les places disponibles");
        printf("\n3 - Retirer une reservation");
        printf("\nChoissez votre action : ");
        scanf("%d", &selection );
    } while ((selection < 1) || (selection > 3));
        return selection;
}
int get_menu_selectionresa(void){
    int selection = 0;
    do{
        system("clear");
        printf("\n1 - Indiquer le nombre de personnes");
        printf("\n2 - Retirer une reservation");
        printf("\nChoissez votre action : ");
        scanf("%d", &selection );
    } while ((selection < 1) || (selection > 3));
        return selection;
}
